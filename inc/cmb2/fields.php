<?php
add_action( 'cmb2_admin_init', 'measurements_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
function measurements_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_measurement_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'metabox2',
		'title'         => __( 'Configure your measurement' ),
		'object_types'  => array( 'measurement', )
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'English label', 'cmb2' ),
		'desc' => __( 'Enter english label of measurement', 'cmb2' ),
		'id'   => $prefix . 'label_en',
		'type' => 'text_medium',
	) );
}