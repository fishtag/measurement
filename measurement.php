<?php
/*
Plugin Name: Measurement
Plugin URI: http://sputnikmobile.com/
Description: Measurement plugin
Version: 1.2
Author: the Sputnik Mobile team
Author URI: http://sputnikmobile.com/
Bitbucket Plugin URI: https://bitbucket.org/fishtag/measurement
Bitbucket Branch: master
*/

/*Some Set-up*/
define('MEASUREMENT_PLUGIN_DIR', WP_PLUGIN_URL . '/' . plugin_basename( dirname(__FILE__) ) . '/' );

/*-------------------------------------------------------------------------------*/
/*   Register Custom Post Types
/*-------------------------------------------------------------------------------*/
add_action( 'init', 'measurement_create_post_type' );
function measurement_create_post_type() {
  register_post_type( 'measurement',
    array(
      'labels' => array(
        'name' => 'Measurement',
        'singular_name' => 'Measurement',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Measurement',
        'edit' => 'Edit',
        'edit_item' => 'Edit Measurement',
        'new_item' => 'New Measurement',
        'view' => 'View',
        'view_item' => 'View Measurement',
        'search_items' => 'Search Measurements',
        'not_found' => 'No Measurements found',
        'not_found_in_trash' => 'No Measurements found in Trash',
        'parent' => 'Parent Measurement'
      ),
      'public' => true,
      'show_ui' => true,
      'publicly_queryable' => true,
      'exclude_from_search' => true,
      'has_archive' => false,
      'hierarchical' => false,
      'rewrite' => array( 'slug' => 'measurement' ),
      'supports' => array( 'title' ),
      'capability_type' => 'page',
      'menu_icon' => 'dashicons-pressthis'
    )
  );
}

/*-------------------------------------------------------------------------------*/
/*   CMB2
/*-------------------------------------------------------------------------------*/
include_once('inc/cmb2/fields.php');